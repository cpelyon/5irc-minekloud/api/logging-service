export interface SearchReponse {
  id: string
  server: number
  message: string[]
  timestamp: Date
}
