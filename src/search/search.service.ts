import { Injectable } from '@nestjs/common'
import { ElasticsearchService } from '@nestjs/elasticsearch'

import { SearchReponse } from './models/search-reponse.interface'

@Injectable()
export class SearchService {
  private maxSizeLogByServer: number
  private maxSizeLogByServerAndTimestamp: number

  constructor(private readonly elasticsearchService: ElasticsearchService) {
    this.maxSizeLogByServer = 50
    this.maxSizeLogByServerAndTimestamp = 25
  }

  async searchByServer(server: number): Promise<SearchReponse[]> {
    const query = {
      bool: {
        must: [
          {
            match: {
              'kubernetes.labels.serverId': server,
            },
          },
          {
            match: {
              'kubernetes.container.name': 'minecraft-server',
            },
          },
        ],
      },
    }

    return this.searchLog(query, this.maxSizeLogByServer)
  }

  async searchByServerAndTimestamp(
    server: number,
    timestamp: any
  ): Promise<SearchReponse[]> {
    const query = {
      bool: {
        must: [
          {
            match: { 'kubernetes.labels.serverId': server },
          },
          {
            match: { 'kubernetes.container.name': 'minecraft-server' },
          },
          {
            range: {
              '@timestamp': timestamp,
            },
          },
        ],
      },
    }
    return this.searchLog(query, this.maxSizeLogByServerAndTimestamp)
  }

  async searchLog(query: any, size = 100): Promise<SearchReponse[]> {
    try {
      const response = await this.elasticsearchService.search({
        index: 'filebeat*',
        body: {
          query,
          fields: ['@timestamp', 'message', 'kubernetes.labels.serverId'],
          _source: 'false',
          sort: { '@timestamp': 'desc' },
        },
        size: size,
      })
      const searchResponse: SearchReponse[] = []
      response.body.hits.hits.map((hit) => {
        searchResponse.push({
          id: String(hit._id),
          server: Number(hit.fields['kubernetes.labels.serverId']),
          message: hit.fields.message,
          timestamp: new Date(hit.fields['@timestamp']),
        })
      })
      return searchResponse.reverse()
    } catch (error) {
      console.log(`error happend : ${error}`)
      return []
    }
  }
}
