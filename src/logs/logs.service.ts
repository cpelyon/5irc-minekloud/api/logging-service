import { Injectable } from '@nestjs/common'
import { Socket } from 'socket.io'
import { Interval } from '@nestjs/schedule'
import { SearchReponse } from 'src/search/models/search-reponse.interface'
import { SearchService } from 'src/search/search.service'
import { ServerLog } from './models/server-log'

@Injectable()
export class LogsService {
  private clientServerLogMap: Map<Socket, ServerLog> = new Map()

  constructor(private readonly searchService: SearchService) {}

  addServerLogToFetch(client: Socket, userId: number, serverId: number) {
    console.log(`Connect: User with ID <${userId}> and server ID <${serverId}>`)
    const serverLog = {
      serverId,
      latestTimestamp: null,
      oldestTimestamp: null,
    }
    this.clientServerLogMap.set(client, serverLog)
    this.fetchLogByServer(client, serverLog)
  }

  removeServerLogToFetch(client: Socket) {
    console.log(`Disconnect: User with socket ID <${client.id}>`)
    this.clientServerLogMap.delete(client)
  }

  sendServerLogToUser(topic: string, client: Socket, logs: SearchReponse[]) {
    const messages = logs.reduce((accumulator, currentValue) => {
      return accumulator.concat(currentValue.message)
    }, [])
    client.emit(topic, messages)
  }

  async fetchLogByServer(client: Socket, serverLog: ServerLog) {
    const logs = await this.searchService.searchByServer(serverLog.serverId)
    this.setServerLogTimestamps(serverLog, logs)
    this.sendServerLogToUser('server-log', client, logs)
  }

  async fetchLogByServerAndTimestamp(
    serverLog: ServerLog,
    filter: any
  ): Promise<SearchReponse[]> {
    const logs = await this.searchService.searchByServerAndTimestamp(
      serverLog.serverId,
      filter
    )
    this.setServerLogTimestamps(serverLog, logs)
    return logs
  }

  async fetchNewLogByServer(client: Socket, serverLog: ServerLog) {
    const filter = { gt: serverLog.latestTimestamp.toISOString() }
    const logs = await this.fetchLogByServerAndTimestamp(serverLog, filter)
    this.sendServerLogToUser('server-log', client, logs)
  }

  async fetchOldLogByServer(client: Socket, serverLog: ServerLog) {
    const filter = { lt: serverLog.oldestTimestamp.toISOString() }
    const logs = await this.fetchLogByServerAndTimestamp(serverLog, filter)
    this.sendServerLogToUser('old-server-log', client, logs)
  }

  setServerLogTimestamps(serverLog: ServerLog, logs: SearchReponse[]) {
    if (logs.length >= 1) {
      const latestLogReceive = logs[logs.length - 1].timestamp
      const oldestLogReceive = logs[0].timestamp

      serverLog.latestTimestamp =
        serverLog.latestTimestamp > latestLogReceive
          ? serverLog.latestTimestamp
          : latestLogReceive

      if(!serverLog.oldestTimestamp) {
        serverLog.oldestTimestamp = oldestLogReceive
        return
      }

      serverLog.oldestTimestamp =
        serverLog.oldestTimestamp < oldestLogReceive
          ? serverLog.oldestTimestamp
          : oldestLogReceive
    }
  }

  getServerLogByClient(client: Socket): ServerLog {
    return this.clientServerLogMap.get(client)
  }

  getOldServerLogByClient(client: Socket) {
    const serverLog: ServerLog = this.clientServerLogMap.get(client)
    this.fetchOldLogByServer(client, serverLog)
  }

  @Interval(Number(process.env.FETCH_LOGS_INTERVAL) || 3000)
  handleIntervalFetchLog() {
    this.clientServerLogMap.forEach(
      async (serverLog: ServerLog, client: Socket) => {
        if (!serverLog.latestTimestamp || !serverLog.oldestTimestamp) {
          this.fetchLogByServer(client, serverLog)
        } else {
          this.fetchNewLogByServer(client, serverLog)
        }
      }
    )
  }
}
