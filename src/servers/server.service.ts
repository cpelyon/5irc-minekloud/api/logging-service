import { Injectable, HttpService } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class ServerService {
  private _baseURL: string

  constructor(
    private httpService: HttpService,
    private configService: ConfigService
  ) {
    this._baseURL = this.configService.get<string>('SERVERS_SERVICE_URL')
  }

  async isUserAllowed(userId: number, serverId: number): Promise<boolean> {
    const server = await this.getServerById(serverId)
    if (server == null || server.idUser != userId) {
      return false
    }
    return true
  }

  async getServerById(idServeur: number) {
    const result = await this.httpService
      .get(this._baseURL + idServeur)
      .toPromise()
      .then((res) => {
        return res.data
      })
      .catch((e) => {
        return null
      })
    return result
  }
}
