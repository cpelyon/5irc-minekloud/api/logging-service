import { HttpModule, Module } from '@nestjs/common'
import { ServerService } from './server.service'

@Module({
  imports: [HttpModule],
  providers: [ServerService],
  exports: [ServerService],
})
export class ServerModule {}
